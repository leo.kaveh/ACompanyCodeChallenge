package com.example.leo.acompanycodechallenge.builtdate;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.leo.acompanycodechallenge.R;
import com.example.leo.acompanycodechallenge.main.MainActivity;
import com.example.leo.acompanycodechallenge.shared.DataContainer;
import com.example.leo.acompanycodechallenge.shared.DependencyContainer;
import com.example.leo.acompanycodechallenge.shared.ui.EndlessRecyclerViewScrollListener;
import com.example.leo.acompanycodechallenge.shared.ui.adapter.MyAdapter;
import com.example.leo.acompanycodechallenge.shared.ui.dialog.MyDialog;

import java.util.ArrayList;

public class BuiltDateFragment extends Fragment implements BuiltDate.View {

    ImageView imageViewErrorIcon;
    TextView textViewErrorMessage;
    Button buttonRetry;
    View layoutError;
    RecyclerView recycleListViewBuiltDate;
    ProgressBar progressbar;
    LinearLayout layoutBreadCrumbs;

    //TODO rename these
    TextView textViewMN;
    TextView textViewMT;


    BuiltDate.Presenter presenter;
    MyAdapter myAdapter;
    String manufacturerId;
    String mainType;
    String manufacturerName;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_built_date, container, false);

        DataContainer dataContainer = DataContainer.getInstance();

        manufacturerId = dataContainer.getManufacturerId();
        manufacturerName = dataContainer.getManufacturerName();
        mainType = dataContainer.getMainType();

        if (manufacturerId == null)
            throw new RuntimeException(getString(R.string.error_manufacturer_id_not_passed));
        if (mainType == null)
            throw new RuntimeException(getString(R.string.error_main_type_not_passed));

        setViews(view);

        createPresenter();
        presenter.onLoadData(manufacturerId, mainType);

        return view;
    }

    private void createPresenter() {
        BuiltDate.Model model = createModel();

        presenter = new BuiltDatePresenterImpl();
        presenter.setView(this);
        presenter.setModel(model);
    }

    private BuiltDate.Model createModel() {
        DependencyContainer dependencyContainer = DependencyContainer.getInstance();

        BuiltDate.Model model = new BuiltDateModelImpl();
        model.setCarTypesClient(dependencyContainer.getCarTypesClient());
        model.setInternetConnectivityHelper(dependencyContainer.getInternetConnectivityHelper());

        return model;
    }

    private void setViews(View view) {
        imageViewErrorIcon = (ImageView) view.findViewById(R.id.image_view_error_icon);
        textViewErrorMessage = (TextView) view.findViewById(R.id.text_view_error_message);
        buttonRetry = (Button) view.findViewById(R.id.button_retry);
        layoutError = view.findViewById(R.id.layout_error);
        recycleListViewBuiltDate = (RecyclerView) view.findViewById(R.id.recycler_view_built_date);
        progressbar = (ProgressBar) view.findViewById(R.id.progressbar);
        layoutBreadCrumbs = (LinearLayout) view.findViewById(R.id.layout_bread_crumbs);
        textViewMN = (TextView) view.findViewById(R.id.text_view_mn);
        textViewMT = (TextView) view.findViewById(R.id.text_view_mt);

        buttonRetry.setOnClickListener((View v) -> presenter.onLoadData(manufacturerId, mainType));

    }

    private void showErrorDialog(String errorMessage) {
        MyDialog myDialog = new MyDialog(getActivity());
        myDialog.setTitle(getString(R.string.error));
        myDialog.setMessage(errorMessage);
        myDialog.setCallback(() -> presenter.onLoadData(manufacturerId,mainType));

        myDialog.show();
    }

    @Override
    public void showInternetNotAvailableError() {
        showErrorDialog(getString(R.string.error_internet_not_available));
    }

    @Override
    public void showInternetNotAvailableErrorOnViewIsEmpty() {
        layoutError.setVisibility(View.VISIBLE);
        textViewErrorMessage.setText(getString(R.string.error_internet_not_available));
        imageViewErrorIcon.setImageResource(R.drawable.ic_no_intenet);
    }

    @Override
    public void showApiFailureError(String errorMessage) {
        showErrorDialog("Error: " + errorMessage);
    }

    @Override
    public void showApiFailureErrorOnViewIsEmpty(String errorMessage) {
        layoutError.setVisibility(View.VISIBLE);
        textViewErrorMessage.setText(getString(R.string.error) + errorMessage);
        imageViewErrorIcon.setImageResource(R.drawable.ic_error);
    }

    @Override
    public void showBuiltDatesList(ArrayList<String> builtDatesList) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recycleListViewBuiltDate.setLayoutManager(linearLayoutManager);

        myAdapter = new MyAdapter(builtDatesList, itemIndex -> {
            presenter.onItemClicked(itemIndex);
        });
        recycleListViewBuiltDate.setAdapter(myAdapter);
        recycleListViewBuiltDate.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                presenter.onLoadData(manufacturerId, mainType);
            }
        });

        textViewMN.setText(manufacturerName);
        textViewMT.setText(mainType);

    }

    @Override
    public void showResultView(String builtDate) {
        DataContainer dataContainer = DataContainer.getInstance();
        dataContainer.setBuiltDate(builtDate);

        ((MainActivity) getActivity()).showResultView();
    }


    @Override
    public void showWaitingView() {
        progressbar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideWaitingView() {
        progressbar.setVisibility(View.GONE);
    }

    @Override
    public void showCompleteMessage() {
        Toast.makeText(getActivity(), getString(R.string.data_complete_message), Toast.LENGTH_LONG).show();
    }

    @Override
    public void hideErrorView() {
        layoutError.setVisibility(View.GONE);
    }
}
