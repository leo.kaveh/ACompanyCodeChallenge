package com.example.leo.acompanycodechallenge.builtdate;


import com.example.leo.acompanycodechallenge.domain.entity.BuiltDateResponse;
import com.example.leo.acompanycodechallenge.domain.rest.cartype.CarTypesCallback;
import com.example.leo.acompanycodechallenge.domain.rest.cartype.CarTypesClient;
import com.example.leo.acompanycodechallenge.shared.CarType;
import com.example.leo.acompanycodechallenge.shared.internetConnectivity.InternetConnectivityHelper;

import java.util.ArrayList;

public class BuiltDateModelImpl implements BuiltDate.Model {

    CarTypesClient carTypesClient;
    InternetConnectivityHelper internetConnectivityHelper;

    ArrayList<String> builtDatesList;

    @Override
    public void setCarTypesClient(CarTypesClient carTypesClient) {
        this.carTypesClient = carTypesClient;
    }

    @Override
    public void setInternetConnectivityHelper(InternetConnectivityHelper internetConnectivityHelper) {
        this.internetConnectivityHelper = internetConnectivityHelper;
    }

    public BuiltDateModelImpl() {
        builtDatesList = new ArrayList<>();
    }


    @Override
    public void loadData(String manufacturerId, String mainType, CarType.ResultListener resultListener) {
        if (!internetConnectivityHelper.isConnected()) {
            resultListener.onInternetIsNotAvailable();
            return;
        }

        if(builtDatesList.size() !=0){
            resultListener.onComplete();
            return;
        }


        carTypesClient.getBuiltDates(manufacturerId, mainType, new CarTypesCallback.BuiltDateCallback() {
            @Override
            public void onBuiltDatesReceived(BuiltDateResponse builtDateResponse) {
                builtDatesList.addAll(builtDateResponse.getAll().values());
                resultListener.onDataLoaded(new ArrayList<>(builtDateResponse.getAll().values()));
            }

            @Override
            public void onApiFailed(Throwable throwable) {
                resultListener.onApiFailed(throwable);
            }
        });
    }

    @Override
    public void onSelectedItemMainTypeRequested(int itemIndex, BuiltDate.BuiltDateListener builtDateListener) {
        String selectedBuiltDate = builtDatesList.get(itemIndex);
        builtDateListener.onSelectedItemBuiltDateReceived(selectedBuiltDate);
    }


}
