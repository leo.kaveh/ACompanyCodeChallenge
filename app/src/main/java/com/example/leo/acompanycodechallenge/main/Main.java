package com.example.leo.acompanycodechallenge.main;


import android.support.v4.app.Fragment;

import com.example.leo.acompanycodechallenge.shared.CarType;

public interface Main extends CarType {


    interface View {
        void replaceView(Fragment newFragment);

        void showManufacturerView();

        void showMainTypeView();

        void replaceViewWithMainTypeView();

        void showBuiltDateView();

        void replaceViewWithBuiltDateView();

        void showResultView();

        void replaceViewWithResultView();

        void enableMainTypeBreadCrumb();

        void enableBuiltDateBreadCrumb();

        void enableResultBreadCrumb();

        void disableMainTypeBreadCrumb();

        void disableBuiltDateBreadCrumb();

        void disableResultBreadCrumb();

        void popBackStack();

        void closeApp();

    }

    interface Presenter {
        void onInitialize();

        void onShowMainTypeViewIsRequested();

        void onShowBuiltDateViewIsRequested();

        void onShowResultViewIsRequested();

        void onBackPressed(int backStackEntryCount);

        void setView(View view);

    }


}
