package com.example.leo.acompanycodechallenge.maintype;


import com.example.leo.acompanycodechallenge.domain.entity.MainTypeResponse;
import com.example.leo.acompanycodechallenge.domain.rest.cartype.CarTypesCallback;
import com.example.leo.acompanycodechallenge.domain.rest.cartype.CarTypesClient;
import com.example.leo.acompanycodechallenge.shared.CarType;
import com.example.leo.acompanycodechallenge.shared.internetConnectivity.InternetConnectivityHelper;

import java.util.ArrayList;

public class MainTypeModelImpl implements MainType.Model {

    CarTypesClient carTypesClient;
    InternetConnectivityHelper internetConnectivityHelper;

    ArrayList<String> mainTypesNameList;
    int pageSize;
    int totalPageCount;
    int pageCount = 0;


    @Override
    public void setCarTypesClient(CarTypesClient carTypesClient) {
        this.carTypesClient = carTypesClient;
    }

    @Override
    public void setInternetConnectivityHelper(InternetConnectivityHelper internetConnectivityHelper) {
        this.internetConnectivityHelper = internetConnectivityHelper;
    }

    public MainTypeModelImpl() {
        mainTypesNameList = new ArrayList<>();
    }

    @Override
    public boolean isFirstItemsAreRepresenting() {
        return (pageCount == 0);
    }


    @Override
    public boolean isAllItemsRepresented() {
        int upperBound = pageSize * totalPageCount;
        int lowerBound = pageSize * (totalPageCount - 1);

        return (mainTypesNameList.size() > lowerBound && mainTypesNameList.size() <= upperBound);
    }



    @Override
    public void loadData(String manufacturerId,CarType.ResultListener resultListener) {
        if (!internetConnectivityHelper.isConnected()) {
            resultListener.onInternetIsNotAvailable();
            return;
        }

        if (isAllItemsRepresented()) {
            resultListener.onComplete();
            return;
        }

        carTypesClient.getMainTypes(manufacturerId, pageCount, new CarTypesCallback.MainTypeCallback() {
            @Override
            public void onMainTypesReceived(MainTypeResponse mainTypeResponse) {
                pageSize = mainTypeResponse.getPageSize();
                totalPageCount = mainTypeResponse.getTotalPageCount();
                mainTypesNameList.addAll(mainTypeResponse.getAll().values());

                resultListener.onDataLoaded(new ArrayList<>(mainTypeResponse.getAll().values()));

                pageCount++;
            }

            @Override
            public void onApiFailed(Throwable throwable) {
                resultListener.onApiFailed(throwable);
            }
        });
    }

    @Override
    public void onSelectedItemMainTypeRequested(int itemIndex,MainType.MainTypeListener mainTypeListener) {
        String selectedMainType = mainTypesNameList.get(itemIndex);
        mainTypeListener.onSelectedItemMainTypeReceived(selectedMainType);
    }

    @Override
    public void loadSearchedData(String searchText, MainType.Presenter presenter) {
        ArrayList<String> searchTypeNameList = new ArrayList<>();

        for (String mainType : mainTypesNameList) {
            if (mainType.toLowerCase().indexOf(searchText.toLowerCase()) != -1)
                searchTypeNameList.add(mainType);
        }

        presenter.onSearchedDataLoaded(searchTypeNameList);

    }

    @Override
    public boolean isListBiggerThanPageSize() {
        return (mainTypesNameList.size() > pageSize);
    }


}
