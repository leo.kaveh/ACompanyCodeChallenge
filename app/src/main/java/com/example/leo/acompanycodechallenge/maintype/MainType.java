package com.example.leo.acompanycodechallenge.maintype;


import com.example.leo.acompanycodechallenge.domain.rest.cartype.CarTypesClient;
import com.example.leo.acompanycodechallenge.shared.CarType;
import com.example.leo.acompanycodechallenge.shared.internetConnectivity.InternetConnectivityHelper;

import java.util.ArrayList;

public interface MainType extends CarType {


    interface View extends CarType.View {
        void showMainTypesList(ArrayList<String> mainTypeList);

        void appendNewMainTypes(ArrayList<String> mainTypeList);

        void showBuiltDatesView(String mainType);

        void showSearchedMainTypesList(ArrayList<String> searchedMainTypeList);
    }


    interface Presenter extends CarType.Presenter {
        void onLoadData(String manufacturerId);

        void onSearch(String searchText);

        void onSearchedDataLoaded(ArrayList<String> searchedMainTypesList);

        void setView(View view);

        void setModel(Model model);
    }


    interface Model extends CarType.Model {

        void loadData(String manufacturerId, ResultListener resultListener);

        void onSelectedItemMainTypeRequested( int itemIndex,MainTypeListener mainTypeListener);

        void loadSearchedData(String searchText, Presenter presenter);

        void setCarTypesClient(CarTypesClient carTypesClient);

        void setInternetConnectivityHelper(InternetConnectivityHelper internetConnectivityHelper);
    }

    interface MainTypeListener{
        void onSelectedItemMainTypeReceived(String selectedMainType);
    }
}
