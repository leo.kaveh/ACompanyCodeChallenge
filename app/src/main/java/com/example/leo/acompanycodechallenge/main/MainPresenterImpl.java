package com.example.leo.acompanycodechallenge.main;


public class MainPresenterImpl implements Main.Presenter {
    Main.View view;

    @Override
    public void setView(Main.View view) {
        this.view = view;
    }

    @Override
    public void onInitialize() {
        view.showManufacturerView();
    }

    @Override
    public void onShowMainTypeViewIsRequested() {
        view.replaceViewWithMainTypeView();
        view.enableMainTypeBreadCrumb();
    }

    @Override
    public void onShowBuiltDateViewIsRequested() {
        view.replaceViewWithBuiltDateView();
        view.enableBuiltDateBreadCrumb();
    }

    @Override
    public void onShowResultViewIsRequested() {
        view.replaceViewWithResultView();
        view.enableResultBreadCrumb();
    }

    @Override
    public void onBackPressed(int backStackEntryCount) {
        if (backStackEntryCount > 0) {
            disableTopBreadCrumb(backStackEntryCount);
            view.popBackStack();
        } else
            view.closeApp();
    }


    private void disableTopBreadCrumb(int backStackEntryCount) {
        switch (backStackEntryCount) {
            case 3:
                view.disableResultBreadCrumb();
                break;
            case 2:
                view.disableBuiltDateBreadCrumb();
                break;
            case 1:
                view.disableMainTypeBreadCrumb();
                break;
        }
    }


}
