package com.example.leo.acompanycodechallenge.shared.ui.adapter;


import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.leo.acompanycodechallenge.R;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private ArrayList<String> dataSet;
    private OnItemClickListener onItemClickListener;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView textViewInfo;
        CardView cardView;
        View view;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            textViewInfo = (TextView) view.findViewById(R.id.text_view_info);
            cardView = (CardView) view.findViewById(R.id.card_view);
        }

        public void bind(final int itemIndex, final OnItemClickListener listener) {
            view.setOnClickListener(v -> listener.onItemClick(itemIndex));
        }
    }

    public MyAdapter(ArrayList<String> dataSet, OnItemClickListener onItemClickListener) {
        this.dataSet = dataSet;
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_row, parent, false);

        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.textViewInfo.setText(dataSet.get(position));

        if(position%2 == 0)
            holder.cardView.setCardBackgroundColor(Color.parseColor("white"));

        else
            holder.cardView.setCardBackgroundColor(Color.parseColor("#fff8f4"));

        holder.bind(position, onItemClickListener);
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public void append(ArrayList<String> newData) {
        dataSet.addAll(newData);
        notifyDataSetChanged();
    }

    public void filter(ArrayList<String> filteredData){
        dataSet = filteredData;
        notifyDataSetChanged();
    }

}
