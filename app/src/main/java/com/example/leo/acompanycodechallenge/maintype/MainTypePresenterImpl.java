package com.example.leo.acompanycodechallenge.maintype;


import com.example.leo.acompanycodechallenge.shared.CarType;

import java.util.ArrayList;

public class MainTypePresenterImpl implements MainType.Presenter {
    MainType.View view;
    MainType.Model model;

    @Override
    public void setView(MainType.View view) {
        this.view = view;
    }

    @Override
    public void setModel(MainType.Model model) {
        this.model = model;
    }


    @Override
    public void onLoadData(String manufacturerId) {
        view.hideErrorView();
        view.showWaitingView();

        model.loadData(manufacturerId, new CarType.ResultListener() {
            @Override
            public void onDataLoaded(ArrayList<String> dataList) {
                view.hideWaitingView();

                if (model.isFirstItemsAreRepresenting())
                    view.showMainTypesList(dataList);
                else {
                    view.appendNewMainTypes(dataList);
                }
            }

            @Override
            public void onInternetIsNotAvailable() {
                view.hideWaitingView();

                if (model.isFirstItemsAreRepresenting())
                    view.showInternetNotAvailableErrorOnViewIsEmpty();
                else
                    view.showInternetNotAvailableError();
            }

            @Override
            public void onComplete() {
                view.hideWaitingView();

                if (model.isListBiggerThanPageSize())
                    view.showCompleteMessage();
            }

            @Override
            public void onApiFailed(Throwable throwable) {
                view.hideWaitingView();

                if (model.isFirstItemsAreRepresenting())
                    view.showApiFailureErrorOnViewIsEmpty(throwable.getMessage());
                else
                    view.showApiFailureError(throwable.getMessage());

            }
        });
    }


    @Override
    public void onSearch(String searchText) {
        model.loadSearchedData(searchText, this);
    }

    @Override
    public void onSearchedDataLoaded(ArrayList<String> searchedMainTypesList) {
        view.showSearchedMainTypesList(searchedMainTypesList);
    }
    
    @Override
    public void onItemClicked(int itemIndex) {
        model.onSelectedItemMainTypeRequested(itemIndex, new MainType.MainTypeListener() {
            @Override
            public void onSelectedItemMainTypeReceived(String selectedMainType) {
                view.showBuiltDatesView(selectedMainType);
            }
        });
    }


}
