package com.example.leo.acompanycodechallenge.maintype;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.leo.acompanycodechallenge.R;
import com.example.leo.acompanycodechallenge.main.MainActivity;
import com.example.leo.acompanycodechallenge.shared.DataContainer;
import com.example.leo.acompanycodechallenge.shared.DependencyContainer;
import com.example.leo.acompanycodechallenge.shared.ui.EndlessRecyclerViewScrollListener;
import com.example.leo.acompanycodechallenge.shared.ui.adapter.MyAdapter;
import com.example.leo.acompanycodechallenge.shared.ui.dialog.MyDialog;

import java.util.ArrayList;


public class MainTypeFragment extends Fragment implements MainType.View {

    ImageView imageViewErrorIcon;
    TextView textViewErrorMessage;
    Button buttonRetry;
    View layoutError;
    RecyclerView recycleListViewMainType;
    ProgressBar progressbar;
    LinearLayout layoutBreadCrumbs;
    TextView textViewManName;
    EditText editTextSearch;

    MainType.Presenter presenter;
    MyAdapter myAdapter;
    String manufacturerId;
    String manufacturerName;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_type, container, false);




        DataContainer dataContainer = DataContainer.getInstance();

        manufacturerId = dataContainer.getManufacturerId();
        manufacturerName = dataContainer.getManufacturerName();
        if (manufacturerId == null)
            throw new RuntimeException(getString(R.string.error_manufacturer_id_not_passed));

        setViews(view);

        createPresenter();
        presenter.onLoadData(manufacturerId);

        return view;
    }

    private void createPresenter() {
        MainType.Model model = createModel();

        presenter = new MainTypePresenterImpl();
        presenter.setModel(model);
        presenter.setView(this);
    }

    private MainType.Model createModel() {
        DependencyContainer dependencyContainer = DependencyContainer.getInstance();

        MainType.Model model = new MainTypeModelImpl();
        model.setInternetConnectivityHelper(dependencyContainer.getInternetConnectivityHelper());
        model.setCarTypesClient(dependencyContainer.getCarTypesClient());

        return model;
    }

    private void setViews(View view) {
        imageViewErrorIcon = (ImageView) view.findViewById(R.id.image_view_error_icon);
        textViewErrorMessage = (TextView) view.findViewById(R.id.text_view_error_message);
        buttonRetry = (Button) view.findViewById(R.id.button_retry);
        layoutError = view.findViewById(R.id.layout_error);
        recycleListViewMainType = (RecyclerView) view.findViewById(R.id.recycler_view_built_date);
        progressbar = (ProgressBar) view.findViewById(R.id.progressbar);
        layoutBreadCrumbs = (LinearLayout) view.findViewById(R.id.layout_bread_crumbs);
        textViewManName = (TextView) view.findViewById(R.id.text_view_bread_crumb_manufacturer);
        editTextSearch = (EditText) view.findViewById(R.id.edit_text_search);

        editTextSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                presenter.onSearch(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        buttonRetry.setOnClickListener((View v) -> presenter.onLoadData(manufacturerId));

    }

    @Override
    public void showInternetNotAvailableError() {
        showErrorDialog(getString(R.string.error_internet_not_available));
    }

    @Override
    public void showInternetNotAvailableErrorOnViewIsEmpty() {
        layoutError.setVisibility(View.VISIBLE);
        textViewErrorMessage.setText(getString(R.string.error_internet_not_available));
        imageViewErrorIcon.setImageResource(R.drawable.ic_no_intenet);
    }

    @Override
    public void showApiFailureError(String errorMessage) {
        showErrorDialog("Error: " + errorMessage);
    }

    @Override
    public void showApiFailureErrorOnViewIsEmpty(String errorMessage) {
        layoutError.setVisibility(View.VISIBLE);
        textViewErrorMessage.setText(getString(R.string.error) + errorMessage);
        imageViewErrorIcon.setImageResource(R.drawable.ic_error);
    }

    @Override
    public void showMainTypesList(ArrayList<String> mainTypeList) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recycleListViewMainType.setLayoutManager(linearLayoutManager);

        myAdapter = new MyAdapter(mainTypeList, itemIndex -> {
            presenter.onItemClicked(itemIndex);
        });
        recycleListViewMainType.setAdapter(myAdapter);
        recycleListViewMainType.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (editTextSearch.getText().length() == 0)
                    presenter.onLoadData(manufacturerId);
            }
        });

        textViewManName.setText(manufacturerName);
        editTextSearch.setVisibility(View.VISIBLE);

    }

    @Override
    public void appendNewMainTypes(ArrayList<String> mainTypeList) {
        myAdapter.append(mainTypeList);
    }

    @Override
    public void showBuiltDatesView(String mainType) {
        DataContainer dataContainer = DataContainer.getInstance();
        dataContainer.setMainType(mainType);

        ((MainActivity) getActivity()).showBuiltDateView();
    }

    @Override
    public void showSearchedMainTypesList(ArrayList<String> searchedMainTypeList) {
        myAdapter.filter(searchedMainTypeList);
    }


    @Override
    public void showWaitingView() {
        progressbar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideWaitingView() {
        progressbar.setVisibility(View.GONE);
    }

    @Override
    public void showCompleteMessage() {
        Toast.makeText(getActivity(), getString(R.string.data_complete_message), Toast.LENGTH_LONG).show();
    }

    @Override
    public void hideErrorView() {
        layoutError.setVisibility(View.GONE);
    }

    private void showErrorDialog(String errorMessage) {
        MyDialog myDialog = new MyDialog(getActivity());
        myDialog.setTitle(getString(R.string.error));
        myDialog.setMessage(errorMessage);
        myDialog.setCallback(() -> presenter.onLoadData(manufacturerId));

        myDialog.show();
    }

}
