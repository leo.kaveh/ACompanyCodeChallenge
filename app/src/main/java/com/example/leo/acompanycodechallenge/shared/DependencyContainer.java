package com.example.leo.acompanycodechallenge.shared;


import android.content.Context;

import com.example.leo.acompanycodechallenge.domain.rest.api.ApiClient;
import com.example.leo.acompanycodechallenge.domain.rest.api.ApiClientImpl;
import com.example.leo.acompanycodechallenge.domain.rest.cartype.CarTypesClient;
import com.example.leo.acompanycodechallenge.domain.rest.cartype.CarTypesClientImpl;
import com.example.leo.acompanycodechallenge.shared.appconfig.AppConfigProvider;
import com.example.leo.acompanycodechallenge.shared.appconfig.AppConfigProviderImpl;
import com.example.leo.acompanycodechallenge.shared.internetConnectivity.InternetConnectivityHelper;
import com.example.leo.acompanycodechallenge.shared.internetConnectivity.InternetConnectivityHelperImpl;

public class DependencyContainer {

    private static DependencyContainer instance;


    private CarTypesClient carTypesClient;
    private InternetConnectivityHelper internetConnectivityHelper;


    public static DependencyContainer getInstance() {
        if (instance == null) {
            instance = new DependencyContainer();
        }
        return instance;
    }

    public void init(Context context) {
        internetConnectivityHelper = new InternetConnectivityHelperImpl();
        internetConnectivityHelper.setContext(context);

        createCarTypesClientObject();
    }

    private void createCarTypesClientObject() {
        AppConfigProvider appConfigProvider = new AppConfigProviderImpl();

        ApiClient apiClient = new ApiClientImpl();
        apiClient.setAppConfigProvider(appConfigProvider);

        carTypesClient = new CarTypesClientImpl();
        carTypesClient.setApiClient(apiClient);
        carTypesClient.setAppConfigProvider(appConfigProvider);
        carTypesClient.setInternetConnectivityHelper(internetConnectivityHelper);
    }

    public CarTypesClient getCarTypesClient() {
        return carTypesClient;
    }

    public InternetConnectivityHelper getInternetConnectivityHelper() {
        return internetConnectivityHelper;
    }
}
