package com.example.leo.acompanycodechallenge.domain.entity;



public class BaseResponse {

    private int page;
    private int pageSize;
    private int totalPageCount;

    public BaseResponse(int page, int pageSize, int totalPageCount) {
        this.page = page;
        this.pageSize = pageSize;
        this.totalPageCount = totalPageCount;
    }

    public int getPage() {
        return page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public int getTotalPageCount() {
        return totalPageCount;
    }
}
