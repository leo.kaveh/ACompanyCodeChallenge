package com.example.leo.acompanycodechallenge.domain.rest.cartype;


import com.example.leo.acompanycodechallenge.domain.rest.api.ApiClient;
import com.example.leo.acompanycodechallenge.shared.appconfig.AppConfigProvider;
import com.example.leo.acompanycodechallenge.shared.internetConnectivity.InternetConnectivityHelper;

public interface CarTypesClient {
    void setApiClient(ApiClient apiClient);

    void setAppConfigProvider(AppConfigProvider appConfigProvider);

    void setInternetConnectivityHelper(InternetConnectivityHelper internetConnectivityHelper);

    void getManufacturers(int pageCount, CarTypesCallback.ManufacturerCallback manufacturerListener);

    void getMainTypes(String manufacturerId, int pageCount, CarTypesCallback.MainTypeCallback mainTypeListener);

    void getBuiltDates(String manufacturerId, String mainType, CarTypesCallback.BuiltDateCallback builtDateListener);
}
