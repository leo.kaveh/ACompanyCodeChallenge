package com.example.leo.acompanycodechallenge.builtdate;


import com.example.leo.acompanycodechallenge.shared.CarType;

import java.util.ArrayList;

public class BuiltDatePresenterImpl implements BuiltDate.Presenter {
    BuiltDate.View view;
    BuiltDate.Model model;

    @Override
    public void setView(BuiltDate.View view) {
        this.view = view;
    }

    @Override
    public void setModel(BuiltDate.Model model) {
        this.model = model;
    }


    @Override
    public void onLoadData(String manufacturerId, String mainType) {
        view.hideErrorView();
        view.showWaitingView();

        model.loadData(manufacturerId, mainType, new CarType.ResultListener() {
            @Override
            public void onDataLoaded(ArrayList<String> dataList) {
                view.hideWaitingView();
                view.showBuiltDatesList(dataList);
            }

            @Override
            public void onInternetIsNotAvailable() {
                view.hideWaitingView();
                view.showInternetNotAvailableErrorOnViewIsEmpty();

            }

            @Override
            public void onComplete() {
                view.hideWaitingView();
            }

            @Override
            public void onApiFailed(Throwable throwable) {
                view.hideWaitingView();
                view.showApiFailureErrorOnViewIsEmpty(throwable.getMessage());

            }
        });
    }







    @Override
    public void onItemClicked(int itemIndex) {
        model.onSelectedItemMainTypeRequested(itemIndex, new BuiltDate.BuiltDateListener() {
            @Override
            public void onSelectedItemBuiltDateReceived(String selectedBuiltDate) {
                view.showResultView(selectedBuiltDate);
            }
        });
    }


}
