package com.example.leo.acompanycodechallenge.manufacturer;


import com.example.leo.acompanycodechallenge.domain.rest.cartype.CarTypesClient;
import com.example.leo.acompanycodechallenge.shared.CarType;
import com.example.leo.acompanycodechallenge.shared.internetConnectivity.InternetConnectivityHelper;

import java.util.ArrayList;

public interface Manufacturer extends CarType{


    interface View extends CarType.View{
        void showManufacturerNamesList(ArrayList<String> manufacturerNamesList);

        void appendNewManufacturerNames(ArrayList<String> manufacturerNamesList);

        void showMainTypesView(String manufacturerId,String manufacturerName);
    }

    interface Presenter extends CarType.Presenter {
        void onLoadData();

        void setView(View view);

        void setModel(Model model);

    }

    interface Model extends CarType.Model {
        void loadData(CarType.ResultListener resultListener);

        void onSelectedItemIdRequested(int itemIndex,ManufacturerListener manufacturerListener);

        void setCarTypesClient(CarTypesClient carTypesClient);

        void setInternetConnectivityHelper(InternetConnectivityHelper internetConnectivityHelper);
    }

    interface ManufacturerListener{
        void onSelectedItemManufacturerReceived(String selectedManufacturerId,String selectedManufacturerName);
    }
}
