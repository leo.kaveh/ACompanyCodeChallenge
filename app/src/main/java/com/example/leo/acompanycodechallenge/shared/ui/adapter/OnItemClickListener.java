package com.example.leo.acompanycodechallenge.shared.ui.adapter;


public interface OnItemClickListener {
    void onItemClick(int itemIndex);
}
