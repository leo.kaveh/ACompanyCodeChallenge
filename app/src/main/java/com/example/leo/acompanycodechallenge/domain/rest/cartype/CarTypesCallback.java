package com.example.leo.acompanycodechallenge.domain.rest.cartype;


import com.example.leo.acompanycodechallenge.domain.entity.BuiltDateResponse;
import com.example.leo.acompanycodechallenge.domain.entity.MainTypeResponse;
import com.example.leo.acompanycodechallenge.domain.entity.ManufacturerResponse;
import com.example.leo.acompanycodechallenge.domain.rest.api.ApiCallback;

public interface CarTypesCallback {

    interface ManufacturerCallback extends ApiCallback {
        void onManufacturerResponseReceived(ManufacturerResponse manufacturerResponse);
    }

    interface MainTypeCallback extends ApiCallback {
        void onMainTypesReceived(MainTypeResponse mainTypeResponse);
    }

    interface BuiltDateCallback extends ApiCallback {
        void onBuiltDatesReceived(BuiltDateResponse builtDateResponse);
    }


}
