package com.example.leo.acompanycodechallenge.domain.rest.api;


import com.example.leo.acompanycodechallenge.domain.entity.BuiltDateResponse;
import com.example.leo.acompanycodechallenge.domain.entity.MainTypeResponse;
import com.example.leo.acompanycodechallenge.domain.entity.ManufacturerResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    @GET("/v1/car-types/manufacturer")
    Call<ManufacturerResponse> getManufacturers(@Query("page") int page,
                                                @Query("pageSize") int pageSize,
                                                @Query("wa_key") String wa_key);

    @GET("/v1/car-types/main-types")
    Call<MainTypeResponse> getMainTypes(@Query("manufacturer") String manufacturerId,
                                        @Query("page") int page,
                                        @Query("pageSize") int pageSize,
                                        @Query("wa_key") String wa_key);

    @GET("/v1/car-types/built-dates")
    Call<BuiltDateResponse> getBuiltDates(@Query("manufacturer") String manufacturerId,
                                          @Query("main-type") String mainType,
                                          @Query("wa_key") String wa_key);
}
