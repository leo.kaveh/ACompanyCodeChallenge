package com.example.leo.acompanycodechallenge.domain.rest.cartype;


import com.example.leo.acompanycodechallenge.domain.entity.BuiltDateResponse;
import com.example.leo.acompanycodechallenge.domain.entity.MainTypeResponse;
import com.example.leo.acompanycodechallenge.domain.entity.ManufacturerResponse;
import com.example.leo.acompanycodechallenge.domain.rest.api.ApiClient;
import com.example.leo.acompanycodechallenge.shared.appconfig.AppConfigProvider;
import com.example.leo.acompanycodechallenge.shared.internetConnectivity.InternetConnectivityHelper;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CarTypesClientImpl implements CarTypesClient {

    ApiClient apiClient;
    AppConfigProvider appConfigProvider;
    InternetConnectivityHelper internetConnectivityHelper;

   @Override
    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    @Override
    public void setAppConfigProvider(AppConfigProvider appConfigProvider) {
        this.appConfigProvider = appConfigProvider;
    }


    @Override
    public void setInternetConnectivityHelper(InternetConnectivityHelper internetConnectivityHelper) {
        this.internetConnectivityHelper = internetConnectivityHelper;
    }


    @Override
    public void getManufacturers(final int pageCount, final CarTypesCallback.ManufacturerCallback manufacturerListener) {
        apiClient.getApiService().getManufacturers(pageCount,
                appConfigProvider.getPageSize(),
                appConfigProvider.getWaKey()).enqueue(new Callback<ManufacturerResponse>() {

            @Override
            public void onResponse(Call<ManufacturerResponse> call, Response<ManufacturerResponse> response) {
                processManufacturerResponse(response, manufacturerListener);
            }

            @Override
            public void onFailure(Call<ManufacturerResponse> call, Throwable t) {
                manufacturerListener.onApiFailed(t);
            }
        });
    }

    @Override
    public void getMainTypes(String manufacturerId, int pageCount, CarTypesCallback.MainTypeCallback mainTypeListener) {
        apiClient.getApiService().getMainTypes(manufacturerId,
                pageCount,
                appConfigProvider.getPageSize(),
                appConfigProvider.getWaKey()).enqueue(new Callback<MainTypeResponse>() {
            @Override
            public void onResponse(Call<MainTypeResponse> call, Response<MainTypeResponse> response) {
                processMainTypeResponse(response, mainTypeListener);
            }

            @Override
            public void onFailure(Call<MainTypeResponse> call, Throwable t) {
                mainTypeListener.onApiFailed(t);
            }
        });

    }

    @Override
    public void getBuiltDates(String manufacturerId, String mainType, CarTypesCallback.BuiltDateCallback builtDateListener) {

        apiClient.getApiService().getBuiltDates(manufacturerId,
                mainType,
                appConfigProvider.getWaKey()).enqueue(new Callback<BuiltDateResponse>() {
            @Override
            public void onResponse(Call<BuiltDateResponse> call, Response<BuiltDateResponse> response) {
                processBuiltDateResponse(response, builtDateListener);
            }

            @Override
            public void onFailure(Call<BuiltDateResponse> call, Throwable t) {
                builtDateListener.onApiFailed(t);
            }
        });
    }

    private void processBuiltDateResponse(Response<BuiltDateResponse> response, CarTypesCallback.BuiltDateCallback builtDateListener) {
        if (response.isSuccessful())
            if (response.body() != null) {
                builtDateListener.onBuiltDatesReceived(response.body());
            } else {
                builtDateListener.onApiFailed(new Throwable("response body is null"));
            }
        else
            builtDateListener.onApiFailed(new Throwable("Unsuccessful - http result code : " + response.code()));

    }

    private void processMainTypeResponse(Response<MainTypeResponse> response, CarTypesCallback.MainTypeCallback mainTypeListener) {
        if (response.isSuccessful())
            if (response.body() != null) {
                mainTypeListener.onMainTypesReceived(response.body());
            } else {
                mainTypeListener.onApiFailed(new Throwable("response body is null"));
            }
        else
            mainTypeListener.onApiFailed(new Throwable("Unsuccessful - http result code : " + response.code()));

    }

    private void processManufacturerResponse(Response<ManufacturerResponse> response, CarTypesCallback.ManufacturerCallback manufacturerListener) {
        if (response.isSuccessful())
            if (response.body() != null) {
                manufacturerListener.onManufacturerResponseReceived(response.body());
            } else {
                manufacturerListener.onApiFailed(new Throwable("response body is null"));
            }
        else
            manufacturerListener.onApiFailed(new Throwable("Unsuccessful - http result code : " + response.code()));

    }
}



