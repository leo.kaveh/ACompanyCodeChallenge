package com.example.leo.acompanycodechallenge.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.leo.acompanycodechallenge.R;
import com.example.leo.acompanycodechallenge.builtdate.BuiltDateFragment;
import com.example.leo.acompanycodechallenge.maintype.MainTypeFragment;
import com.example.leo.acompanycodechallenge.manufacturer.ManufacturerFragment;
import com.example.leo.acompanycodechallenge.result.ResultFragment;

public class MainActivity extends AppCompatActivity implements Main.View {

    Main.Presenter presenter;
    FragmentTransaction transaction;
    LinearLayout linearLayoutBreadCrumbs;

    TextView textViewBreadCrumbMainType;
    TextView textViewBreadCrumbBuiltDate;
    TextView textViewBreadCrumbResult;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setViews();

        createPresenter();
        presenter.onInitialize();

    }

    private void createPresenter() {
        presenter = new MainPresenterImpl();
        presenter.setView(this);
    }
    private void setViews() {
        linearLayoutBreadCrumbs = (LinearLayout) findViewById(R.id.layout_bread_crumbs);

        textViewBreadCrumbMainType = (TextView) linearLayoutBreadCrumbs.findViewById(R.id.text_view_bread_crumb_main_type);
        textViewBreadCrumbBuiltDate = (TextView) linearLayoutBreadCrumbs.findViewById(R.id.text_view_bread_crumb_built_date);
        textViewBreadCrumbResult = (TextView) linearLayoutBreadCrumbs.findViewById(R.id.text_view_bread_crumb_result);
    }

    @Override
    public void replaceView(Fragment newFragment) {
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
        transaction.replace(R.id.frl_fragment_container, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void showManufacturerView() {
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.frl_fragment_container, new ManufacturerFragment());
        transaction.commit();
    }


    @Override
    public void showMainTypeView() {
        presenter.onShowMainTypeViewIsRequested();
    }

    @Override
    public void replaceViewWithMainTypeView() {
        replaceView(new MainTypeFragment());
    }

    @Override
    public void showBuiltDateView() {
        presenter.onShowBuiltDateViewIsRequested();
    }

    @Override
    public void replaceViewWithBuiltDateView() {
        replaceView(new BuiltDateFragment());
    }

    @Override
    public void showResultView() {
        presenter.onShowResultViewIsRequested();
    }

    @Override
    public void replaceViewWithResultView() {
        replaceView(new ResultFragment());
    }

    @Override
    public void enableMainTypeBreadCrumb() {
        textViewBreadCrumbMainType.setTextColor(ContextCompat.getColor(this, R.color.white));
    }

    @Override
    public void enableBuiltDateBreadCrumb() {
        textViewBreadCrumbBuiltDate.setTextColor(ContextCompat.getColor(this, R.color.white));
    }

    @Override
    public void enableResultBreadCrumb() {
        textViewBreadCrumbResult.setTextColor(ContextCompat.getColor(this, R.color.white));
    }

    @Override
    public void disableMainTypeBreadCrumb() {
        textViewBreadCrumbMainType.setTextColor(ContextCompat.getColor(this, R.color.blue_grey));
    }

    @Override
    public void disableBuiltDateBreadCrumb() {
        textViewBreadCrumbBuiltDate.setTextColor(ContextCompat.getColor(this, R.color.blue_grey));
    }

    @Override
    public void disableResultBreadCrumb() {
        textViewBreadCrumbResult.setTextColor(ContextCompat.getColor(this, R.color.blue_grey));
    }



    @Override
    public void popBackStack() {
        getSupportFragmentManager().popBackStack();
    }

    @Override
    public void closeApp() {
        finish();
    }


    @Override
    public void onBackPressed() {
        int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
        presenter.onBackPressed(backStackEntryCount);
    }


}
