package com.example.leo.acompanycodechallenge.result;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.leo.acompanycodechallenge.R;
import com.example.leo.acompanycodechallenge.shared.DataContainer;

public class ResultFragment extends Fragment {
    String manufacturerName;
    String mainType;
    String builtDate;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_result, container, false);

        DataContainer dataContainer = DataContainer.getInstance();

        manufacturerName = dataContainer.getManufacturerName();
        mainType = dataContainer.getMainType();
        builtDate = dataContainer.getBuiltDate();

        if (manufacturerName == null)
            throw new RuntimeException(getString(R.string.error_manufacturer_name_not_passed));
        if (mainType == null)
            throw new RuntimeException(getString(R.string.error_main_type_not_passed));
        if (builtDate == null)
            throw new RuntimeException(getString(R.string.error_built_date_not_passed));

        setViews(view);

        return view;
    }

    private void setViews(View view) {
        TextView textViewManufacturer = (TextView) view.findViewById(R.id.text_view_manufacturer);
        TextView textViewMainType = (TextView) view.findViewById(R.id.text_view_main_type);
        TextView textViewBuiltDate = (TextView) view.findViewById(R.id.text_view_bread_crumb_built_date);

        textViewManufacturer.setText(manufacturerName);
        textViewMainType.setText(mainType);
        textViewBuiltDate.setText(builtDate);
    }
}
