package com.example.leo.acompanycodechallenge.builtdate;


import com.example.leo.acompanycodechallenge.domain.rest.cartype.CarTypesClient;
import com.example.leo.acompanycodechallenge.shared.CarType;
import com.example.leo.acompanycodechallenge.shared.internetConnectivity.InternetConnectivityHelper;

import java.util.ArrayList;

public interface BuiltDate extends CarType {


    interface View extends CarType.View {
        void showBuiltDatesList(ArrayList<String> builtDatesList);

        void showResultView(String builtDate);
    }

    interface Presenter extends CarType.Presenter {
        void onLoadData(String manufacturerId, String mainType);

        void setView(View view);

        void setModel(Model model);
    }


    interface Model  {

        void loadData(String manufacturerId, String mainType, CarType.ResultListener resultListener);

        void onSelectedItemMainTypeRequested(int itemIndex, BuiltDateListener builtDateListener);

        void setCarTypesClient(CarTypesClient carTypesClient);

        void setInternetConnectivityHelper(InternetConnectivityHelper internetConnectivityHelper);
    }

    interface BuiltDateListener {
        void onSelectedItemBuiltDateReceived(String selectedBuiltDate);
    }
}
