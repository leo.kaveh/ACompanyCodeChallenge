package com.example.leo.acompanycodechallenge.manufacturer;


import com.example.leo.acompanycodechallenge.domain.entity.ManufacturerResponse;
import com.example.leo.acompanycodechallenge.domain.rest.cartype.CarTypesCallback;
import com.example.leo.acompanycodechallenge.domain.rest.cartype.CarTypesClient;
import com.example.leo.acompanycodechallenge.shared.CarType;
import com.example.leo.acompanycodechallenge.shared.internetConnectivity.InternetConnectivityHelper;

import java.util.ArrayList;

public class ManufacturerModelImpl implements Manufacturer.Model {

    CarTypesClient carTypesClient;
    InternetConnectivityHelper internetConnectivityHelper;

    ArrayList<String> manufacturerNameList;
    ArrayList<String> manufacturerIdList;
    int pageSize;
    int totalPageCount;
    int pageCount = 0;

    @Override
    public void setCarTypesClient(CarTypesClient carTypesClient) {
        this.carTypesClient = carTypesClient;
    }

    @Override
    public void setInternetConnectivityHelper(InternetConnectivityHelper internetConnectivityHelper) {
        this.internetConnectivityHelper = internetConnectivityHelper;
    }

    public ManufacturerModelImpl() {
        manufacturerIdList = new ArrayList<>();
        manufacturerNameList = new ArrayList<>();
    }

    @Override
    public boolean isFirstItemsAreRepresenting() {
        return (pageCount == 0);
    }


    @Override
    public boolean isAllItemsRepresented() {
        int upperBound = pageSize * totalPageCount;
        int lowerBound = pageSize * (totalPageCount - 1);

        return (manufacturerNameList.size() > lowerBound && manufacturerNameList.size() <= upperBound);
    }

    @Override
    public boolean isListBiggerThanPageSize() {
        return (manufacturerIdList.size() > pageSize);
    }


    @Override
    public void loadData(CarType.ResultListener resultListener) {
        if (!internetConnectivityHelper.isConnected()) {
            resultListener.onInternetIsNotAvailable();
            return;
        }

        if (isAllItemsRepresented()) {
            resultListener.onComplete();
            return;
        }

        carTypesClient.getManufacturers(pageCount, new CarTypesCallback.ManufacturerCallback() {
            @Override
            public void onManufacturerResponseReceived(ManufacturerResponse manufacturerResponse) {
                pageSize = manufacturerResponse.getPageSize();
                totalPageCount = manufacturerResponse.getTotalPageCount();
                manufacturerNameList.addAll(manufacturerResponse.getAll().values());
                manufacturerIdList.addAll(manufacturerResponse.getAll().keySet());

                resultListener.onDataLoaded(new ArrayList<>(manufacturerResponse.getAll().values()));

                pageCount++;


            }

            @Override
            public void onApiFailed(Throwable throwable) {
                resultListener.onApiFailed(throwable);
            }
        });
    }

    @Override
    public void onSelectedItemIdRequested(int itemIndex,Manufacturer.ManufacturerListener manufacturerListener) {
        String selectedManufacturerId = manufacturerIdList.get(itemIndex);
        String selectedManufacturerName = manufacturerNameList.get(itemIndex);

        manufacturerListener.onSelectedItemManufacturerReceived(selectedManufacturerId,selectedManufacturerName);
    }


}
