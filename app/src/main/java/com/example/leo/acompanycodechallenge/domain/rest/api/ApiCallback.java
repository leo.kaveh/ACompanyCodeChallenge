package com.example.leo.acompanycodechallenge.domain.rest.api;


public interface ApiCallback {
    void onApiFailed(Throwable throwable);
}
