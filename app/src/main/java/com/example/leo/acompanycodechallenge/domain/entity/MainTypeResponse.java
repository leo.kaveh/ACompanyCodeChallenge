package com.example.leo.acompanycodechallenge.domain.entity;


import java.util.Map;

public class MainTypeResponse extends BaseResponse {

    private Map<String, String> wkda;

    public MainTypeResponse(int page, int pageSize, int totalPageCount, Map<String, String> wkda) {
        super(page, pageSize, totalPageCount);
        this.wkda = wkda;
    }

    public Map<String, String> getAll() {
        return wkda;
    }
}
