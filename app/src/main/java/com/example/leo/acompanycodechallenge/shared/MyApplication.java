package com.example.leo.acompanycodechallenge.shared;

import android.app.Application;

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        DependencyContainer.getInstance().init(getApplicationContext());
    }

}
