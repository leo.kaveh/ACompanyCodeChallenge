package com.example.leo.acompanycodechallenge.shared.internetConnectivity;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


public class InternetConnectivityHelperImpl implements InternetConnectivityHelper {

    Context context;

    @Override
    public void setContext(Context context) {
        this.context = context;
    }

    @Override
    public boolean isConnected() {
        final ConnectivityManager conMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
        return (activeNetwork != null && activeNetwork.isConnected());
    }


}
