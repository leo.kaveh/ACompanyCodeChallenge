package com.example.leo.acompanycodechallenge.manufacturer;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.leo.acompanycodechallenge.R;
import com.example.leo.acompanycodechallenge.main.MainActivity;
import com.example.leo.acompanycodechallenge.shared.DataContainer;
import com.example.leo.acompanycodechallenge.shared.DependencyContainer;
import com.example.leo.acompanycodechallenge.shared.ui.EndlessRecyclerViewScrollListener;
import com.example.leo.acompanycodechallenge.shared.ui.adapter.MyAdapter;
import com.example.leo.acompanycodechallenge.shared.ui.dialog.MyDialog;

import java.util.ArrayList;


public class ManufacturerFragment extends Fragment implements Manufacturer.View {

    ImageView imageViewErrorIcon;
    TextView textViewErrorMessage;
    Button buttonRetry;
    View layoutError;
    RecyclerView recycleListViewManufacturer;
    ProgressBar progressbar;

    Manufacturer.Presenter presenter;
    MyAdapter myAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_manufacturer, container, false);

        setViews(view);

        createPresenter();
        presenter.onLoadData();

        return view;
    }

    private void createPresenter() {
        Manufacturer.Model model = createModel();

        presenter = new ManufacturerPresenterImpl();
        presenter.setModel(model);
        presenter.setView(this);
    }

    private Manufacturer.Model createModel() {
        DependencyContainer dependencyContainer = DependencyContainer.getInstance();

        Manufacturer.Model model = new ManufacturerModelImpl();
        model.setCarTypesClient(dependencyContainer.getCarTypesClient());
        model.setInternetConnectivityHelper(dependencyContainer.getInternetConnectivityHelper());

        return model;
    }
    private void setViews(View view) {
        imageViewErrorIcon = (ImageView) view.findViewById(R.id.image_view_error_icon);
        textViewErrorMessage = (TextView) view.findViewById(R.id.text_view_error_message);
        buttonRetry = (Button) view.findViewById(R.id.button_retry);
        layoutError = view.findViewById(R.id.layout_error);
        recycleListViewManufacturer = (RecyclerView) view.findViewById(R.id.recycler_view_manufacturer);
        progressbar = (ProgressBar) view.findViewById(R.id.progressbar);

        buttonRetry.setOnClickListener((View v) -> presenter.onLoadData());
    }

    @Override
    public void showInternetNotAvailableError() {
        showErrorDialog(getString(R.string.error_internet_not_available));
    }

    @Override
    public void showInternetNotAvailableErrorOnViewIsEmpty() {
        layoutError.setVisibility(View.VISIBLE);
        textViewErrorMessage.setText(getString(R.string.error_internet_not_available));
        imageViewErrorIcon.setImageResource(R.drawable.ic_no_intenet);
    }

    @Override
    public void showApiFailureError(String errorMessage) {
        showErrorDialog("Error: " + errorMessage);
    }

    @Override
    public void showApiFailureErrorOnViewIsEmpty(String errorMessage) {
        layoutError.setVisibility(View.VISIBLE);
        textViewErrorMessage.setText(getString(R.string.error) + errorMessage);
        imageViewErrorIcon.setImageResource(R.drawable.ic_error);
    }

    @Override
    public void showManufacturerNamesList(ArrayList<String> manufacturerNamesList) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recycleListViewManufacturer.setLayoutManager(linearLayoutManager);

        myAdapter = new MyAdapter(manufacturerNamesList, itemIndex -> presenter.onItemClicked(itemIndex));
        recycleListViewManufacturer.setAdapter(myAdapter);
        recycleListViewManufacturer.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                presenter.onLoadData();
            }
        });
    }

    @Override
    public void appendNewManufacturerNames(ArrayList<String> manufacturerNamesList) {
        myAdapter.append(manufacturerNamesList);
    }

    @Override
    public void showMainTypesView(String manufacturerId, String manufacturerName) {
        DataContainer dataContainer = DataContainer.getInstance();
        dataContainer.setManufacturerId(manufacturerId);
        dataContainer.setManufacturerName(manufacturerName);

        ((MainActivity) getActivity()).showMainTypeView();
    }


    @Override
    public void showWaitingView() {
        progressbar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideWaitingView() {
        progressbar.setVisibility(View.GONE);
    }

    @Override
    public void showCompleteMessage() {
        Toast.makeText(getActivity(), getString(R.string.data_complete_message), Toast.LENGTH_LONG).show();
    }

    @Override
    public void hideErrorView() {
        layoutError.setVisibility(View.GONE);
    }

    private void showErrorDialog(String errorMessage) {
        MyDialog myDialog = new MyDialog(getActivity());
        myDialog.setTitle(getString(R.string.error));
        myDialog.setMessage(errorMessage);
        myDialog.setCallback(() -> presenter.onLoadData());

        myDialog.show();
    }
}
