package com.example.leo.acompanycodechallenge.domain.rest.api;


import com.example.leo.acompanycodechallenge.shared.appconfig.AppConfigProvider;

public interface ApiClient {
    ApiService getApiService();

    void setAppConfigProvider(AppConfigProvider appConfigProvider);
}