package com.example.leo.acompanycodechallenge.shared.appconfig;


public interface AppConfigProvider {
    String getWaKey();

    int getPageSize();

    String getBaseUrl();

    String getManufacturerIdKey();

    String  getManufacturerNameKey();

    String getMainTypeKey();

    String getBuiltDateKey();
}
