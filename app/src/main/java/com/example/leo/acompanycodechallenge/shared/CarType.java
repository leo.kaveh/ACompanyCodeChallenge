package com.example.leo.acompanycodechallenge.shared;


import java.util.ArrayList;

public interface CarType {

    interface Presenter {

        void onItemClicked(int itemIndex);

    }

    interface View {

        void showInternetNotAvailableError();

        void showInternetNotAvailableErrorOnViewIsEmpty();

        void showApiFailureError(String errorMessage);

        void showApiFailureErrorOnViewIsEmpty(String errorMessage);

        void showWaitingView();

        void hideWaitingView();

        void showCompleteMessage();

        void hideErrorView();
    }

    interface Model {
        boolean isFirstItemsAreRepresenting();

        boolean isAllItemsRepresented();

        boolean isListBiggerThanPageSize();

    }

    interface ResultListener{
        void onDataLoaded(ArrayList<String> dataList);

        void onInternetIsNotAvailable();

        void onComplete();

        void onApiFailed(Throwable throwable);
    }

}
