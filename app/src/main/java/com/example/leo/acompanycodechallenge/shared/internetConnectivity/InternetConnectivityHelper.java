package com.example.leo.acompanycodechallenge.shared.internetConnectivity;


import android.content.Context;

public interface InternetConnectivityHelper {
    boolean isConnected();

    void setContext(Context context);


}
