package com.example.leo.acompanycodechallenge.domain.rest.api;


import com.example.leo.acompanycodechallenge.shared.appconfig.AppConfigProvider;
import com.google.gson.Gson;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClientImpl implements ApiClient{

    AppConfigProvider appConfigProvider;

    @Override
    public void setAppConfigProvider(AppConfigProvider appConfigProvider) {
        this.appConfigProvider = appConfigProvider;
    }

    @Override
    public ApiService getApiService() {
        return getRetrofit().create(ApiService.class);
    }

    private Retrofit getRetrofit() {
        return (new Retrofit.Builder())
                .baseUrl(appConfigProvider.getBaseUrl())
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .build();
    }
}
