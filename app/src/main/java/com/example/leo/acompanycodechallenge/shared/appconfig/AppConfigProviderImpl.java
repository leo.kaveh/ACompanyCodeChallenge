package com.example.leo.acompanycodechallenge.shared.appconfig;



public class AppConfigProviderImpl implements AppConfigProvider {
    private static final String BASE_URL = "http://api-aws-eu-qa-1.auto1-test.com";
    private static final String WA_KEY = "coding-puzzle-client-449cc9d";
    private static final int PAGE_SIZE= 15;
    private static final String MANUFACTURER_ID_KEY = "ManufacturerIdKey";
    private static final String MANUFACTURER_NAME_KEY = "ManufacturerNameKey";
    private static final String MAIN_TYPE_KEY = "MainTypeKey";
    private static final String BUILT_DATE_KEY = "BuiltDateKey";


    @Override
    public String getWaKey() {
        return WA_KEY;
    }

    @Override
    public int getPageSize() {
        return PAGE_SIZE;
    }

    @Override
    public String getBaseUrl() {
        return BASE_URL;
    }

    @Override
    public String getManufacturerIdKey() {
        return MANUFACTURER_ID_KEY;
    }

    @Override
    public String getManufacturerNameKey() {
        return MANUFACTURER_NAME_KEY;
    }

    @Override
    public String getMainTypeKey() {
        return MAIN_TYPE_KEY;
    }

    @Override
    public String getBuiltDateKey() {
        return BUILT_DATE_KEY;
    }
}
