package com.example.leo.acompanycodechallenge.manufacturer;


import com.example.leo.acompanycodechallenge.shared.CarType;

import java.util.ArrayList;

public class ManufacturerPresenterImpl implements Manufacturer.Presenter {
    Manufacturer.View view;
    Manufacturer.Model model;

    @Override
    public void setView(Manufacturer.View view) {
        this.view = view;
    }

    @Override
    public void setModel(Manufacturer.Model model) {
        this.model = model;
    }

    @Override
    public void onLoadData() {
        view.hideErrorView();
        view.showWaitingView();

        model.loadData(new CarType.ResultListener() {
            @Override
            public void onDataLoaded(ArrayList<String> dataList) {
                view.hideWaitingView();

                if (model.isFirstItemsAreRepresenting())
                    view.showManufacturerNamesList(dataList);
                else {
                    view.appendNewManufacturerNames(dataList);
                }
            }

            @Override
            public void onInternetIsNotAvailable() {
                view.hideWaitingView();

                if (model.isFirstItemsAreRepresenting())
                    view.showInternetNotAvailableErrorOnViewIsEmpty();
                else
                    view.showInternetNotAvailableError();
            }

            @Override
            public void onComplete() {
                view.hideWaitingView();

                if (model.isListBiggerThanPageSize())
                    view.showCompleteMessage();
            }

            @Override
            public void onApiFailed(Throwable throwable) {
                view.hideWaitingView();

                if (model.isFirstItemsAreRepresenting())
                    view.showApiFailureErrorOnViewIsEmpty(throwable.getMessage());
                else
                    view.showApiFailureError(throwable.getMessage());
            }
        });
    }


    @Override
    public void onItemClicked(int itemIndex) {
        model.onSelectedItemIdRequested(itemIndex, new Manufacturer.ManufacturerListener() {
            @Override
            public void onSelectedItemManufacturerReceived(String selectedManufacturerId, String selectedManufacturerName) {
                view.showMainTypesView(selectedManufacturerId, selectedManufacturerName);
            }
        });
    }



}
