package com.example.leo.acompanycodechallenge.shared.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.example.leo.acompanycodechallenge.R;

import butterknife.ButterKnife;


public class MyDialog extends Dialog {

    TextView textViewTitle;
    TextView textViewMessage;
    Button buttonRetry;


    Context context;
    MyDialogCallback myDialogCallback;
    String title;
    String message;

    public void setCallback(MyDialogCallback myDialogCallback) {
        this.myDialogCallback = myDialogCallback;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public MyDialog(Context context) {
        super(context);
        this.context = context;

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.layout_dialog);
        ButterKnife.bind(this);
        setCanceledOnTouchOutside(false);
        setViews();
        getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);


    }

    @Override
    public void show() {
        textViewTitle.setText(title);
        textViewMessage.setText(message);
        super.show();
    }

    private void setViews(){
        textViewTitle = (TextView)findViewById(R.id.text_view_title);
        textViewMessage = (TextView)findViewById(R.id.text_view_message);
        buttonRetry = (Button)findViewById(R.id.button_retry);

        buttonRetry.setOnClickListener(view -> {
            myDialogCallback.onRetry();
            dismiss();
        });
    }
}
